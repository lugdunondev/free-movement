Namespace.declare("net.lugdunon.state.character.movement.free");
Namespace.require("net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding");
Namespace.newClass("net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement","net.lugdunon.state.character.movement.IDirectionalMovement");

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_NONE               =0;
net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_NORTH              =1;
net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_EAST               =2;
net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_SOUTH              =4;
net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_WEST               =8;

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME=135.0;

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement,"init",[initData]);
	
	this.freeMovementBits=0;
	this.transitiveLoc   ={x:0,y:0,sx:0,sy:0};
	
	return(this);
};

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.prototype.getKeyBindingImplementation=function()
{
	return("net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding");
};

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.prototype.isDirectionallyMoving=function()
{
	return(this.freeMovementBits != net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_NONE);
};

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.prototype.handleServerSideMovementUpdate=function(res)
{
	var y=this.character.actualLoc.y;
	
	this.character.setLocation       (res.readUint16 (),res.readUint16 ());
	this.character.setSubTileLocation(res.readFloat64(),res.readFloat64());

	if(this.character == game.player)
	{	
		game.updateScreenLoc();
		
		if(res.readBoolean())
		{
			game.chunkManager.chunksUpdated(res,true);
		}
		
		game.mainPlayerMoved();
	}

	if(this.character.actualLoc.y != y)
	{
		game.sortCharacters();
	}
};

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.prototype.handleDirectionalMovement=function(delta)
{

};



////



net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.prototype.clearFreeMovementBits=function()
{
	this.setFreeMovementBits(net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_NONE);
};

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.prototype.setFreeMovementBits=function(freeMovementBits)
{
	this.freeMovementBits=freeMovementBits;
	
	//STOP WALKING
	if(this.freeMovementBits == 0)
	{
		this.character.setOrientation();

		//SYNC UP
		this.character.loc. x=this.character.actualLoc. x;
		this.character.loc. y=this.character.actualLoc. y;
		this.character.loc.sx=this.character.actualLoc.sx;
		this.character.loc.sy=this.character.actualLoc.sy;
		
		if(this.character == game.player)
		{
			game.mainPlayerMoved();
		}
	}
	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_NORTH) != 0)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_NORTH;
		
		if(this.character.sprite != null)
		{
	   	 	this.character.sprite.setFrameSet("walkUp");
		}
	}
	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_SOUTH) != 0)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_SOUTH;
		
		if(this.character.sprite != null)
		{
			this.character.sprite.setFrameSet("walkDown");
		}
	}
	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_EAST)  != 0)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_EAST;
		
		if(this.character.sprite != null)
		{
			this.character.sprite.setFrameSet("walkRight");
		}
	}
	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.DIRECTION_BIT_WEST)  != 0)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_WEST;
		
		if(this.character.sprite != null)
		{
   	 		this.character.sprite.setFrameSet("walkLeft");
		}
	}
};

////

net.lugdunon.state.character.movement.free.legacy.FreeNonInterpolatedDirectionalMovement.prototype.fromBytes=function(res)
{
	this.setFreeMovementBits(res.readUint8());
};
