package net.lugdunon.state.character.movement.free.legacy;

import java.io.ByteArrayOutputStream;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.command.ex.IsNotServerInvokedCommand;
import net.lugdunon.io.EnhancedDataOutputStream;
import net.lugdunon.math.Point;
import net.lugdunon.math.SubTileLocation;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.character.movement.IDirectionalMovement;
import net.lugdunon.util.FastMath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FreeNonInterpolatedDirectionalMovement implements IDirectionalMovement
{
	
	public static final int    DIRECTION_BIT_NONE           =0;
	public static final int    DIRECTION_BIT_NORTH          =1;
	public static final int    DIRECTION_BIT_EAST           =2;
	public static final int    DIRECTION_BIT_SOUTH          =4;
	public static final int    DIRECTION_BIT_WEST           =8;
	
	public static final double FREE_MOVE_TILE_TRAVERSAL_TIME=135.0;

	private Logger    logger;
	
	private Character character;
	private byte      freeMovementBits;
	
	public FreeNonInterpolatedDirectionalMovement()
	{
		logger          =LoggerFactory.getLogger(FreeNonInterpolatedDirectionalMovement.class);
		freeMovementBits=0;
	}


	@Override
    public void setCharacter(Character character)
    {
	    this.character=character;
    }
	
	@Override
    public Character getCharacter()
    {
	    return(character);
    }
	
	@Override
    public boolean isDirectionallyMoving()
    {
		return(freeMovementBits != DIRECTION_BIT_NONE);
    }
	
	@Override
	public void directionalMovementInitiated(int direction)
	{
		switch(direction)
		{
			case DIRECTION_NONE:
			{
				setFreeMovementBit(DIRECTION_BIT_NONE, true);
				break;
			}
			case DIRECTION_NORTH:
			{
				setFreeMovementBit(DIRECTION_BIT_NORTH, true);
				break;
			}
			case DIRECTION_EAST:
			{
				setFreeMovementBit(DIRECTION_BIT_EAST, true);
				break;
			}
			case DIRECTION_SOUTH:
			{
				setFreeMovementBit(DIRECTION_BIT_SOUTH, true);
				break;
			}
			case DIRECTION_WEST:
			{
				setFreeMovementBit(DIRECTION_BIT_WEST, true);
				break;
			}
		}
	}

	@Override
	public void directionalMovementCanceled(int direction)
	{
		switch(direction)
		{
			case DIRECTION_NONE:
			{
				setFreeMovementBit(DIRECTION_BIT_NONE, false);
				break;
			}
			case DIRECTION_NORTH:
			{
				setFreeMovementBit(DIRECTION_BIT_NORTH, false);
				break;
			}
			case DIRECTION_EAST:
			{
				setFreeMovementBit(DIRECTION_BIT_EAST, false);
				break;
			}
			case DIRECTION_SOUTH:
			{
				setFreeMovementBit(DIRECTION_BIT_SOUTH, false);
				break;
			}
			case DIRECTION_WEST:
			{
				setFreeMovementBit(DIRECTION_BIT_WEST, false);
				break;
			}
		}
	}

	@Override
	public void cancelAllDirectionalMovement()
	{
		freeMovementBits = DIRECTION_BIT_NONE;
	}

	@Override
	public void handleDirectionalMovement(long delta)
	{
		boolean         tto=false;
    	SubTileLocation stp=new SubTileLocation(getCharacter().getSubTileLocation());
    	
	    //NORTH
	    if((freeMovementBits & DIRECTION_BIT_NORTH) != 0)
	    {
	    	stp.addY(-delta/FREE_MOVE_TILE_TRAVERSAL_TIME);
	    	
	    	//NORTHWEST
	     	if((freeMovementBits & DIRECTION_BIT_WEST) != 0)
	     	{
	     		stp.addX(-delta/FREE_MOVE_TILE_TRAVERSAL_TIME);
	     	}
	     	//NORTHEAST
	     	else if((freeMovementBits & DIRECTION_BIT_EAST) != 0)
	     	{
	     		stp.addX( delta/FREE_MOVE_TILE_TRAVERSAL_TIME);
	     	}
	    }
	    //SOUTH
	    else if((this.freeMovementBits & DIRECTION_BIT_SOUTH) != 0)
	    {
	    	stp.addY( delta/FREE_MOVE_TILE_TRAVERSAL_TIME);
	    	
	    	//SOUTHWEST
	     	if((freeMovementBits & DIRECTION_BIT_WEST) != 0)
	     	{
	     		stp.addX(-delta/FREE_MOVE_TILE_TRAVERSAL_TIME);
	     	}
	     	//SOUTHEAST
	     	else if((freeMovementBits & DIRECTION_BIT_EAST) != 0)
	     	{
	     		stp.addX( delta/FREE_MOVE_TILE_TRAVERSAL_TIME);
	     	}
	    }
	    //WEST
	    else if((freeMovementBits & DIRECTION_BIT_WEST)  != 0)
	    {
	    	stp.addX(-delta/FREE_MOVE_TILE_TRAVERSAL_TIME);
	    }
	    //EAST
	    else if((freeMovementBits & DIRECTION_BIT_EAST)  != 0)
	    {
	    	stp.addX( delta/FREE_MOVE_TILE_TRAVERSAL_TIME);
	    }
	    
	    // CHECK FOR TILE TRANSITION
	    if(stp.getX() == 1.0 || stp.getX() == -1.0 || stp.getY() == 1.0 || stp.getY() == -1.0)
	    {
	    	Point p=new Point(getCharacter().getLocation());
	    	
	    	//L
	    	if(stp.getX() == -1.0)
	    	{
	    		p  .addX(-1);
	    		stp.setX( 0.99);
	    		
	    		//TL
	    		if(stp.getY() == -1.0)
	    		{
	    			p  .addY(-1);
	    			stp.setY( 0.99);
	    		}
	    		//BL
	    		else if(stp.getY() == 1.0)
	    		{
	    			p  .addY( 1);
	    			stp.setY(-0.99);
	    		}
	    	}
	    	//R
	    	else if(stp.getX() == 1.0)
	    	{
	    		p  .addX( 1);
	    		stp.setX(-0.99);
	    		
	    		//TR
	    		if(stp.getY() == -1.0)
	    		{
	    			p  .addY(-1);
	    			stp.setY(0.99);
	    		}
	    		//BR
	    		else if(stp.getY() == 1.0)
	    		{
	    			p  .addY( 1);
	    			stp.setY(-0.99);
	    		}
	    	}
	    	//T
	    	else if(stp.getY() == -1.0)
	    	{
	    		p  .addY(-1);
	    		stp.setY( 0.99);
	    	}
	    	//B
	    	else if(stp.getY() == 1.0)
	    	{
	    		p  .addY( 1);
	    		stp.setY(-0.99);
	    	}
	    	
	    	p.setX(FastMath.wrap(p.getX(),State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));
	    	p.setY(FastMath.wrap(p.getY(),State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));
	    	
	    	try
	    	{
	    		if(!State.instance().getWorld().getInstance(getCharacter().getInstanceId()).isTileImpassable(p))
	    		{
	    			tto=true;
	    			
	    			getCharacter().setLocation       (p  );
	    			getCharacter().setSubTileLocation(stp);
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		;
	    	}
	    }
	    //CHECK FOR TILE SIDE TRANSITION
	    else if(
	    	(getCharacter().getSubTileLocation().getX() <=  0.5 && stp.getX() >  0.5) || 
	    	(getCharacter().getSubTileLocation().getX() >= -0.5 && stp.getX() < -0.5) || 
	    	(getCharacter().getSubTileLocation().getY() <=  0.5 && stp.getY() >  0.5) || 
	    	(getCharacter().getSubTileLocation().getY() >= -0.5 && stp.getY() < -0.5)
	    )
	    {
	    	Point p=new Point(getCharacter().getLocation());
	    	
	    	//L
	    	if(getCharacter().getSubTileLocation().getX() >= -0.5 && stp.getX() < -0.5)
	    	{
	    		p.addX(-1);
	    		
	    		//TL
	    		if(getCharacter().getSubTileLocation().getY() >= -0.5 && stp.getY() < -0.5)
	    		{
	    			p.addY(-1);
	    		}
	    		//BL
	    		else if(getCharacter().getSubTileLocation().getY() <= 0.5 && stp.getY() > 0.5)
	    		{
	    			p.addY( 1);
	    		}
	    	}
	    	//R
	    	else if(getCharacter().getSubTileLocation().getX() <= 0.5 && stp.getX() > 0.5)
	    	{
	    		p.addX( 1);
	    		
	    		//TR
	    		if(getCharacter().getSubTileLocation().getY() >= -0.5 && stp.getY() < -0.5)
	    		{
	    			p.addY(-1);
	    		}
	    		//BR
	    		else if(getCharacter().getSubTileLocation().getY() <= 0.5 && stp.getY() > 0.5)
	    		{
	    			p.addY( 1);
	    		}
	    	}
	    	//T
	    	else if(getCharacter().getSubTileLocation().getY() >= -0.5 && stp.getY() < -0.5)
	    	{
	    		p.addY(-1);
	    	}
	    	//B
	    	else if(getCharacter().getSubTileLocation().getY() <= 0.5 && stp.getY() > 0.5)
	    	{
	    		p.addY( 1);
	    	}
	    	
	    	p.setX(FastMath.wrap(p.getX(),State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));
	    	p.setY(FastMath.wrap(p.getY(),State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));
	    	
	    	try
	    	{
	    		if(!State.instance().getWorld().getInstance(getCharacter().getInstanceId()).isTileImpassable(p))
	    		{
	    			getCharacter().setSubTileLocation(stp);
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		;
	    	}
	    }
	    else
	    {
	    	getCharacter().setSubTileLocation(stp);
	    }
	    
	    try
	    {
	    	CommandProperties props=new CommandProperties();
	    	
	    	props.setCharacter      ("character",             getCharacter()                     );
	    	props.setPoint          ("location",              getCharacter().getLocation()       );
	    	props.setSubTileLocation("subTileLocation",       getCharacter().getSubTileLocation());
	    	props.setBoolean        ("cancelPath",            false                              );
	    	props.setBoolean        ("tileTransitionOccurred",tto                                );
	    	
	        State.instance().getGame().addIncomingRequest(
	    		"CORE.COMMAND.PLAYER.MOVE",
	    		props
	        );
	    }
	    catch (CommandNotSupportedException e)
	    {
	        e.printStackTrace();
	    }
	    catch (IsNotServerInvokedCommand e)
	    {
	        e.printStackTrace();
	    }
	}
	
	@Override
	public byte findOrientation()
    {
	    //NORTH
	    if((freeMovementBits & DIRECTION_BIT_NORTH) != 0)
	    {
	    	return(Character.ORIENTATION_NORTH);
	    }
	    //SOUTH
	    else if((this.freeMovementBits & DIRECTION_BIT_SOUTH) != 0)
	    {
	    	return(Character.ORIENTATION_SOUTH);
	    }
	    //WEST
	    else if((freeMovementBits & DIRECTION_BIT_WEST)  != 0)
	    {
	    	return(Character.ORIENTATION_WEST);
	    }
	    //EAST
	    else if((freeMovementBits & DIRECTION_BIT_EAST)  != 0)
	    {
	    	return(Character.ORIENTATION_EAST);
	    }
	    
	    return(Character.ORIENTATION_SOUTH);
    }

	@Override
    public byte[] toBytes()
    {
	    EnhancedDataOutputStream out=null;
	    
		try
		{
		    ByteArrayOutputStream baos=new ByteArrayOutputStream();
		    
		    out=new EnhancedDataOutputStream(baos);

		    out.writeByte(freeMovementBits);
		    
		    return(baos.toByteArray());
		}
		catch(Exception e)
		{
			logger.error(e.getMessage(),e);
		}
		finally
		{
			try{out.close();}catch(Exception e){};
		}
		
		return(null);
    }
	
	////
	
	public boolean isValidFreeMovementDirection(int bit)
    {
	    return(
	    	(bit == DIRECTION_BIT_NORTH) ||
	    	(bit == DIRECTION_BIT_SOUTH) ||
	    	(bit == DIRECTION_BIT_EAST ) ||
	    	(bit == DIRECTION_BIT_WEST )
	    );
    }

	public byte setFreeMovementBit(int bit, boolean set)
	{
		if(isValidFreeMovementDirection(bit))
		{
			if(set)
			{
				freeMovementBits|=bit;
			}
			else
			{
				freeMovementBits&=0x0F-bit;
			}
			
			if(bit == DIRECTION_BIT_EAST && hasFreeMovementBit(DIRECTION_BIT_WEST))
			{
				freeMovementBits=setFreeMovementBit(DIRECTION_BIT_WEST,false);
			}
			else if(bit == DIRECTION_BIT_WEST && hasFreeMovementBit(DIRECTION_BIT_EAST))
			{
				freeMovementBits=setFreeMovementBit(DIRECTION_BIT_EAST,false);
			}
			else if(bit == DIRECTION_BIT_NORTH && hasFreeMovementBit(DIRECTION_BIT_SOUTH))
			{
				freeMovementBits=setFreeMovementBit(DIRECTION_BIT_SOUTH,false);
			}
			else if(bit == DIRECTION_BIT_SOUTH && hasFreeMovementBit(DIRECTION_BIT_NORTH))
			{
				freeMovementBits=setFreeMovementBit(DIRECTION_BIT_NORTH,false);
			}
		}
	
		return(freeMovementBits);
	}
	
	private boolean hasFreeMovementBit(int bit)
	{
		return((freeMovementBits&bit) == 1);
	}
}