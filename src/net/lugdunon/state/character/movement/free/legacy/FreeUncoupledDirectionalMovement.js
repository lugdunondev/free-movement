Namespace.declare("net.lugdunon.state.character.movement.free");
Namespace.require("net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding");
Namespace.newClass("net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement","net.lugdunon.state.character.movement.IDirectionalMovement");

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_NONE               =0;
net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_NORTH              =1;
net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_EAST               =2;
net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_SOUTH              =4;
net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_WEST               =8;

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME=135.0;

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement,"init",[initData]);
	
	this.freeMovementBits=0;
	this.transitiveLoc   ={x:0,y:0,sx:0,sy:0};
	
	return(this);
};

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.prototype.getKeyBindingImplementation=function()
{
	return("net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding");
};

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.prototype.isDirectionallyMoving=function()
{
	return(this.freeMovementBits != net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_NONE);
};

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.prototype.handleServerSideMovementUpdate=function(res)
{
	var y=this.character.actualLoc.y;
	
	res.readUint16 ();res.readUint16 ();
	res.readFloat64();res.readFloat64();
	
	if(this.character == game.player)
	{	
		if(res.readBoolean())
		{
			game.chunkManager.chunksUpdated(res,true);

		}

		game.updateScreenLoc();
		game.mainPlayerMoved();
	}

	if(this.character.actualLoc.y != y)
	{
		game.sortCharacters();
	}
};

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.prototype.handleDirectionalMovement=function(delta)
{
	this.transitiveLoc. x=this.character.loc. x;
	this.transitiveLoc. y=this.character.loc. y;
	this.transitiveLoc.sx=this.character.loc.sx;
	this.transitiveLoc.sy=this.character.loc.sy;

    //NORTH
    if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_NORTH) != 0)
    {
    	this.transitiveLoc.sy+=(-delta/net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME);
    	
    	//NORTHWEST
     	if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_WEST) != 0)
     	{
     		this.transitiveLoc.sx+=(-delta/net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME);
     	}
     	//NORTHEAST
     	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_EAST) != 0)
     	{
     		this.transitiveLoc.sx+=( delta/net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME);
     	}
    }
    //SOUTH
    else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_SOUTH) != 0)
    {
    	this.transitiveLoc.sy+=( delta/net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME);
    	
    	//SOUTHWEST
     	if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_WEST) != 0)
     	{
     		this.transitiveLoc.sx+=(-delta/net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME);
     	}
     	//SOUTHEAST
     	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_EAST) != 0)
     	{
     		this.transitiveLoc.sx+=( delta/net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME);
     	}
    }
    //WEST
    else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_WEST)  != 0)
    {
    	this.transitiveLoc.sx+=(-delta/net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME);
    }
    //EAST
    else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_EAST)  != 0)
    {
    	this.transitiveLoc.sx+=( delta/net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME);
    }
    
    // CHECK FOR TILE TRANSITION
    if(this.transitiveLoc.sx >= 1.0 || this.transitiveLoc.sx <= -1.0 || this.transitiveLoc.sy >= 1.0 || this.transitiveLoc.sy <= -1.0)
    {
    	//L
    	if(this.transitiveLoc.sx <= -1.0)
    	{
    		this.transitiveLoc. x+=(-1);
    		this.transitiveLoc.sx =( 0.99);
    		
    		//TL
    		if(this.transitiveLoc.sy <= -1.0)
    		{
    			this.transitiveLoc.y+=(-1);
    			this.transitiveLoc.sy =( 0.99);
    		}
    		//BL
    		else if(this.transitiveLoc.sy >= 1.0)
    		{
    			this.transitiveLoc. y+=( 1);
    			this.transitiveLoc.sy =(-0.99);
    		}
    	}
    	//R
    	else if(this.transitiveLoc.sx >= 1.0)
    	{
    		this.transitiveLoc. x+=( 1);
    		this.transitiveLoc.sx =(-0.99);
    		
    		//TR
    		if(this.transitiveLoc.sy <= -1.0)
    		{
    			this.transitiveLoc. y+=(-1);
    			this.transitiveLoc.sy =(0.99);
    		}
    		//BR
    		else if(this.transitiveLoc.sy >= 1.0)
    		{
    			this.transitiveLoc. y+=( 1);
    			this.transitiveLoc.sy =(-0.99);
    		}
    	}
    	//T
    	else if(this.transitiveLoc.sy <= -1.0)
    	{
    		this.transitiveLoc. y+=(-1);
    		this.transitiveLoc.sy =( 0.99);
    	}
    	//B
    	else if(this.transitiveLoc.sy >= 1.0)
    	{
    		this.transitiveLoc. y+=( 1);
    		this.transitiveLoc.sy =(-0.99);
    	}

    	this.transitiveLoc.x=net.lugdunon.util.Math.wrap(this.transitiveLoc.x,game.tileSetDef.mapSize);
    	this.transitiveLoc.y=net.lugdunon.util.Math.wrap(this.transitiveLoc.y,game.tileSetDef.mapSize);
    	
    	try
    	{
    		if(!game.chunkManager.isImpassable(this.transitiveLoc))
    		{
    	    	this.character.setLocation       (this.transitiveLoc. x,this.transitiveLoc. y);
    	    	this.character.setSubTileLocation(this.transitiveLoc.sx,this.transitiveLoc.sy);
    		}
    	}
    	catch(e)
    	{
    		;
    	}
    }
    else
    {  		
    	this.character.setSubTileLocation(this.transitiveLoc.sx,this.transitiveLoc.sy);
    }

	if(this.character == game.player)
	{
		if(this.character.footsteps == null)
		{
			this.character.footsteps=assetManager.getAudio("FOOTSTEPS",null,true);
		}
		
		if(!this.character.footsteps.isPlaying())
		{
			this.character.footsteps.setVolume(game.getSoundVolume());
			this.character.footsteps.play();
		}

		game.updateScreenLoc();
		game.mainPlayerMoved();
	}
};



////



net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.prototype.clearFreeMovementBits=function()
{
	this.setFreeMovementBits(net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_NONE);
};

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.prototype.setFreeMovementBits=function(freeMovementBits)
{
	this.freeMovementBits=freeMovementBits;
	
	//STOP WALKING
	if(this.freeMovementBits == 0)
	{
		this.character.setOrientation();

		//SYNC UP
		this.character.loc. x=this.character.actualLoc. x;
		this.character.loc. y=this.character.actualLoc. y;
		this.character.loc.sx=this.character.actualLoc.sx;
		this.character.loc.sy=this.character.actualLoc.sy;
		
		if(this.character == game.player)
		{
			game.mainPlayerMoved();
		}
	}
	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_NORTH) != 0)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_NORTH;
		
		if(this.character.sprite != null)
		{
	   	 	this.character.sprite.setFrameSet("walkUp");
		}
	}
	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_SOUTH) != 0)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_SOUTH;
		
		if(this.character.sprite != null)
		{
			this.character.sprite.setFrameSet("walkDown");
		}
	}
	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_EAST)  != 0)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_EAST;
		
		if(this.character.sprite != null)
		{
			this.character.sprite.setFrameSet("walkRight");
		}
	}
	else if((this.freeMovementBits & net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.DIRECTION_BIT_WEST)  != 0)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_WEST;
		
		if(this.character.sprite != null)
		{
   	 		this.character.sprite.setFrameSet("walkLeft");
		}
	}
};

////

net.lugdunon.state.character.movement.free.legacy.FreeUncoupledDirectionalMovement.prototype.fromBytes=function(res)
{
	this.setFreeMovementBits(res.readUint8());
};
