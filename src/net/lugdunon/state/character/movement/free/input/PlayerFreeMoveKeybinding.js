Namespace.declare("net.lugdunon.state.character.movement.free.input");
Namespace.newClass("net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding","net.lugdunon.input.keybind.Keybinding");

net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.prototype.init=function(initData)
{
	this.triggered             =false;
	this.direction             =initData.direction.direction;

	initData.id                =this.classId+"."+initData.direction.id+this.direction;
	initData.inputModes        =net.lugdunon.input.keybind.Keybinding.INPUT_MODE_UP|net.lugdunon.input.keybind.Keybinding.INPUT_MODE_DOWN;
	initData.defaultKeybinding ={
		primaryKeyCode  :initData.direction.defaultPrimaryKeyCode,
		secondaryKeyCode:initData.direction.defaultSecondaryKeyCode
	};
	initData.label             ="Move "+initData.direction.text;

	this.callSuper(net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding,"init",[initData]);
	
	return(this);
};

net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.prototype.handle=function(inputMode)
{
	if(inputMode == net.lugdunon.input.keybind.Keybinding.INPUT_MODE_DOWN)
	{
		if(!this.triggered && game.gameMode == 0)
		{
			this.triggered=true;
			game.client.sendCommand(game.client.buildCommand("CORE.COMMAND.PLAYER.DIRECTIONAL.MOVE",{direction:this.direction,start:true}));
		}
	}
	else if(inputMode == net.lugdunon.input.keybind.Keybinding.INPUT_MODE_UP)
	{
		this.triggered=false;
		game.client.sendCommand(game.client.buildCommand("CORE.COMMAND.PLAYER.DIRECTIONAL.MOVE",{direction:this.direction,start:false}));
	}
};

//net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CONTINUE=false;
//net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CUR_DIR =0;
//
//net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.startRandomWalk=function()
//{
//	if(!net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CONTINUE)
//	{
//		net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CONTINUE=true;
//		net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.nextRandomWalk();
//	}
//};
//
//net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.stopRandomWalk=function()
//{
//	net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CONTINUE=false;
//	game.client.sendCommand(game.client.buildCommand("CORE.COMMAND.PLAYER.FREE.MOVE",{direction:net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CUR_DIR,start:false}));
//};
//
//net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.nextRandomWalk=function()
//{
//	if(net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CONTINUE)
//	{
//		game.client.sendCommand(game.client.buildCommand("CORE.COMMAND.PLAYER.FREE.MOVE",{direction:net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CUR_DIR,start:false}));
//		
//		net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CUR_DIR=1<<Math.round(Math.random()*3);
//		
//console.log(game.player.freeMovementBits+" "+net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CUR_DIR);		
//
//		game.client.sendCommand(game.client.buildCommand("CORE.COMMAND.PLAYER.FREE.MOVE",{direction:net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.CUR_DIR,start:true }));
//		
//		setTimeout(function(){net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding.nextRandomWalk();},1000);
//	}
//};