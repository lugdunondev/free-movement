Namespace.declare("net.lugdunon.state.character.movement.free");
Namespace.require("net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding");
Namespace.newClass("net.lugdunon.state.character.movement.free.FreeDirectionalMovement","net.lugdunon.state.character.movement.IDirectionalMovement");

net.lugdunon.state.character.movement.free.FreeDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME=135.0;
net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD =  0.2;

net.lugdunon.state.character.movement.free.FreeDirectionalMovement.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.state.character.movement.free.FreeDirectionalMovement,"init",[initData]);
	
	this.currentDirection =[];
//	this.avgUpdateInterval={l:0,c:300,a:1};
	this.tileTraversalTime=0;
	this.transitiveLoc    ={x:0,y:0,sx:0,sy:0};
	
	return(this);
};

net.lugdunon.state.character.movement.free.FreeDirectionalMovement.prototype.getKeyBindingImplementation=function()
{
	return("net.lugdunon.state.character.movement.free.input.PlayerFreeMoveKeybinding");
};

net.lugdunon.state.character.movement.free.FreeDirectionalMovement.prototype.isDirectionallyMoving=function()
{
	return(this.currentDirection != net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_NONE);
};

net.lugdunon.state.character.movement.free.FreeDirectionalMovement.prototype.handleServerSideMovementUpdate=function(res)
{
	var y=this.character.actualLoc.y;
	
	this.character.setLocation       (res.readUint16 (),res.readUint16 ());
	this.character.setSubTileLocation(res.readFloat64(),res.readFloat64());

	if(this.character == game.player)
	{	
		if(res.readBoolean())
		{
			game.chunkManager.chunksUpdated(res,true);
		}
		
		if(res.readBoolean())
		{
			game.chunkManager.biomeDistanceUpdate(res);
		}

		game.updateScreenLoc();
		game.mainPlayerMoved();
	}

	if(this.character.actualLoc.y != y)
	{
		game.sortCharacters();
	}
};

net.lugdunon.state.character.movement.free.FreeDirectionalMovement.prototype.handleDirectionalMovement=function(delta)
{
	if(this.currentDirection != net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_NONE)
	{
		this.tileTraversalTime=Math.floor(
			net.lugdunon.state.character.movement.free.FreeDirectionalMovement.FREE_MOVE_TILE_TRAVERSAL_TIME*
			this.character.characterStats.getMovementSpeed()
		)+game.getLatency();
		
		this.transitiveLoc. x=this.character.actualLoc. x;
		this.transitiveLoc. y=this.character.actualLoc. y;
		this.transitiveLoc.sx=this.character.actualLoc.sx;
		this.transitiveLoc.sy=this.character.actualLoc.sy;
		
	    //NORTH
	    if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_NORTH)
	    {
	    	this.transitiveLoc.sy+=(-delta/this.tileTraversalTime);
	    }
	    //SOUTH
	    else if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_SOUTH)
	    {
	    	this.transitiveLoc.sy+=( delta/this.tileTraversalTime);
	    }
	    //WEST
	    else if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_WEST)
	    {
	    	this.transitiveLoc.sx+=(-delta/this.tileTraversalTime);
	    }
	    //EAST
	    else if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_EAST)
	    {
	    	this.transitiveLoc.sx+=( delta/this.tileTraversalTime);
	    }
	    
	    if(this.transitiveLoc.sx > net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD)
	    {
	    	this.transitiveLoc.x+=( 1);
	    	
	    	if(game.chunkManager.isImpassable(this.transitiveLoc))
	    	{
	    		this.transitiveLoc.sx= net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD;
	    	}
	    	
	    	this.transitiveLoc.x-=( 1);
	    }
	    else if(this.transitiveLoc.sx < -net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD)
	    {
	    	this.transitiveLoc.x+=(-1);
	    	
	    	if(game.chunkManager.isImpassable(this.transitiveLoc))
	    	{
	    		this.transitiveLoc.sx=-net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD;
	    	}
	    	
	    	this.transitiveLoc.x-=(-1);
	    }
	    else if(this.transitiveLoc.sy > net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD)
	    {
	    	this.transitiveLoc.y+=( 1);
	    	
	    	if(game.chunkManager.isImpassable(this.transitiveLoc))
	    	{
	    		this.transitiveLoc.sy= net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD;
	    	}
	    	
	    	this.transitiveLoc. y-=( 1);
	    }
	    else if(this.transitiveLoc.sy < -net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD)
	    {
	    	this.transitiveLoc.y+=(-1);
	    	
	    	if(game.chunkManager.isImpassable(this.transitiveLoc))
	    	{
	    		this.transitiveLoc.sy=-net.lugdunon.state.character.movement.free.FreeDirectionalMovement.NEXT_TO_IMPASSABLE_THRESHOLD;
	    	}
	    	
	    	this.transitiveLoc.y-=(-1);
	    }
	    
	    // CHECK FOR TILE TRANSITION
	    if(this.transitiveLoc.sx >= 1.0 || this.transitiveLoc.sx <= -1.0 || this.transitiveLoc.sy >= 1.0 || this.transitiveLoc.sy <= -1.0)
	    {
	    	//L
	    	if(this.transitiveLoc.sx <= -1.0)
	    	{
	    		this.transitiveLoc. x+=(-1);
	    		this.transitiveLoc.sx =( 0.99);
	    	}
	    	//R
	    	else if(this.transitiveLoc.sx >= 1.0)
	    	{
	    		this.transitiveLoc. x+=( 1);
	    		this.transitiveLoc.sx =(-0.99);
	    	}
	    	//T
	    	else if(this.transitiveLoc.sy <= -1.0)
	    	{
	    		this.transitiveLoc. y+=(-1);
	    		this.transitiveLoc.sy =( 0.99);
	    	}
	    	//B
	    	else if(this.transitiveLoc.sy >= 1.0)
	    	{
	    		this.transitiveLoc. y+=( 1);
	    		this.transitiveLoc.sy =(-0.99);
	    	}
	
	    	this.transitiveLoc.x=net.lugdunon.util.Math.wrap(this.transitiveLoc.x,game.tileSetDef.mapSize);
	    	this.transitiveLoc.y=net.lugdunon.util.Math.wrap(this.transitiveLoc.y,game.tileSetDef.mapSize);
	    }
	    
		if(!game.chunkManager.isImpassable(this.transitiveLoc))
		{
	    	this.character.setLocation       (this.transitiveLoc. x,this.transitiveLoc. y);
	        this.character.setSubTileLocation(this.transitiveLoc.sx,this.transitiveLoc.sy);
		}
		
		/////
	
		if(this.character == game.player)
		{
			if(this.character.footsteps == null)
			{
				this.character.footsteps=assetManager.getAudio("FOOTSTEPS",null,true);
			}
			
			if(!this.character.footsteps.isPlaying())
			{
				this.character.footsteps.setVolume(game.getSoundVolume());
				this.character.footsteps.play();
			}
	
			game.updateScreenLoc();
			game.mainPlayerMoved();
		}
	}
};

////

net.lugdunon.state.character.movement.free.FreeDirectionalMovement.prototype.fromBytes=function(res)
{
	this.currentDirection=res.readUint8();

	//STOP WALKING
	if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_NONE)
	{
		this.character.setOrientation();

		//SYNC UP
		this.character.loc. x=this.character.actualLoc. x;
		this.character.loc. y=this.character.actualLoc. y;
		this.character.loc.sx=this.character.actualLoc.sx;
		this.character.loc.sy=this.character.actualLoc.sy;
		
		if(this.character == game.player)
		{
			game.mainPlayerMoved();
		}
	}
	else if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_NORTH)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_NORTH;
		
		if(this.character.sprite != null)
		{
	   	 	this.character.sprite.setFrameSet("walkUp");
		}
	}
	else if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_SOUTH)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_SOUTH;
		
		if(this.character.sprite != null)
		{
			this.character.sprite.setFrameSet("walkDown");
		}
	}
	else if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_EAST)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_EAST;
		
		if(this.character.sprite != null)
		{
			this.character.sprite.setFrameSet("walkRight");
		}
	}
	else if(this.currentDirection == net.lugdunon.state.character.movement.IDirectionalMovement.DIRECTION_WEST)
	{
		this.character.orientation=net.lugdunon.character.Character.ORIENTATION_WEST;
		
		if(this.character.sprite != null)
		{
   	 		this.character.sprite.setFrameSet("walkLeft");
		}
	}
};
