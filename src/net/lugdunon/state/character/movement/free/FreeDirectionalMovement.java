package net.lugdunon.state.character.movement.free;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.command.ex.IsNotServerInvokedCommand;
import net.lugdunon.io.EnhancedDataOutputStream;
import net.lugdunon.math.Point;
import net.lugdunon.math.SubTileLocation;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.character.movement.IDirectionalMovement;
import net.lugdunon.util.FastMath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FreeDirectionalMovement implements IDirectionalMovement
{
	public static final double FREE_MOVE_TILE_TRAVERSAL_TIME=135.0;
	public static final double NEXT_TO_IMPASSABLE_THRESHOLD =  0.2;

	private Logger        logger;
	private Character     character;
	private List<Integer> movementStack;
	
	public FreeDirectionalMovement()
	{
		logger       =LoggerFactory.getLogger(FreeDirectionalMovement.class);
		movementStack=new ArrayList<Integer>();
	}
	
	@Override
    public void setCharacter(Character character)
    {
	    this.character=character;
    }
	
	@Override
    public Character getCharacter()
    {
	    return(character);
    }
	
	@Override
    public boolean isDirectionallyMoving()
    {
		return(movementStack.size() > 0);
    }
	
	@Override
	public void directionalMovementInitiated(int direction)
	{
		if(
			(direction == IDirectionalMovement.DIRECTION_NORTH) ||
			(direction == IDirectionalMovement.DIRECTION_SOUTH) ||
			(direction == IDirectionalMovement.DIRECTION_EAST ) ||
			(direction == IDirectionalMovement.DIRECTION_WEST )
		)
		{
			if(movementStack.contains(direction))
			{
				directionalMovementCanceled(direction);
			}
			
			movementStack.add(0,direction);
		}
	}

	@Override
	public void directionalMovementCanceled(int direction)
	{
		movementStack.remove(new Integer(direction));
	}

	@Override
	public void cancelAllDirectionalMovement()
	{
		movementStack.clear();
	}

	@Override
	public void handleDirectionalMovement(long delta)
	{
		if(isDirectionallyMoving())
		{
			boolean         tto=false;
			int             dir=movementStack.get(0);
	    	SubTileLocation stp=new SubTileLocation(getCharacter().getSubTileLocation());
	    	
			character.setOrientation(findOrientation(),true);
	    	
		    //NORTH
		    if(dir == IDirectionalMovement.DIRECTION_NORTH)
		    {
		    	stp.addY(-delta/(FREE_MOVE_TILE_TRAVERSAL_TIME*getCharacter().getCharacterStats().getMovementSpeed()));
		    }
		    //SOUTH
		    else if(dir == IDirectionalMovement.DIRECTION_SOUTH)
		    {
		    	stp.addY( delta/(FREE_MOVE_TILE_TRAVERSAL_TIME*getCharacter().getCharacterStats().getMovementSpeed()));
		    }
		    //WEST
		    else if(dir == IDirectionalMovement.DIRECTION_WEST)
		    {
		    	stp.addX(-delta/(FREE_MOVE_TILE_TRAVERSAL_TIME*getCharacter().getCharacterStats().getMovementSpeed()));
		    }
		    //EAST
		    else if(dir == IDirectionalMovement.DIRECTION_EAST)
		    {
		    	stp.addX( delta/(FREE_MOVE_TILE_TRAVERSAL_TIME*getCharacter().getCharacterStats().getMovementSpeed()));
		    }
		    
		    if(stp.getX() > NEXT_TO_IMPASSABLE_THRESHOLD)
		    {
		    	Point p=new Point(getCharacter().getLocation());

		    	p.setX(FastMath.wrap(p.getX()+1,State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));

		    	if(State.instance().getWorld().getInstance(getCharacter().getInstanceId()).isTileImpassable(p))
		    	{
		    		stp.setX( NEXT_TO_IMPASSABLE_THRESHOLD);
		    	}
		    }
		    else if(stp.getX() < -NEXT_TO_IMPASSABLE_THRESHOLD)
		    {
		    	Point p=new Point(getCharacter().getLocation());

		    	p.setX(FastMath.wrap(p.getX()-1,State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));
		    	
		    	if(State.instance().getWorld().getInstance(getCharacter().getInstanceId()).isTileImpassable(p))
		    	{
		    		stp.setX(-NEXT_TO_IMPASSABLE_THRESHOLD);
		    	}
		    }
		    else if(stp.getY() > NEXT_TO_IMPASSABLE_THRESHOLD)
		    {
		    	Point p=new Point(getCharacter().getLocation());

		    	p.setY(FastMath.wrap(p.getY()+1,State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));

		    	if(State.instance().getWorld().getInstance(getCharacter().getInstanceId()).isTileImpassable(p))
		    	{
		    		stp.setY( NEXT_TO_IMPASSABLE_THRESHOLD);
		    	}
		    }
		    else if(stp.getY() < -NEXT_TO_IMPASSABLE_THRESHOLD)
		    {
		    	Point p=new Point(getCharacter().getLocation());
		    	
		    	p.setY(FastMath.wrap(p.getY()-1,State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));
		    	
		    	if(State.instance().getWorld().getInstance(getCharacter().getInstanceId()).isTileImpassable(p))
		    	{
		    		stp.setY(-NEXT_TO_IMPASSABLE_THRESHOLD);
		    	}
		    }
		    
		    // CHECK FOR TILE TRANSITION
		    if(stp.getX() >= 1.0 || stp.getX() <= -1.0 || stp.getY() >= 1.0 || stp.getY() <= -1.0)
		    {
		    	Point p=new Point(getCharacter().getLocation());
		    	
		    	//L
		    	if(stp.getX() <= -1.0)
		    	{
		    		p  .addX(-1);
		    		stp.setX( 0.99);
		    	}
		    	//R
		    	else if(stp.getX() >= 1.0)
		    	{
		    		p  .addX( 1);
		    		stp.setX(-0.99);
		    	}
		    	//T
		    	else if(stp.getY() <= -1.0)
		    	{
		    		p  .addY(-1);
		    		stp.setY( 0.99);
		    	}
		    	//B
		    	else if(stp.getY() >= 1.0)
		    	{
		    		p  .addY( 1);
		    		stp.setY(-0.99);
		    	}
		    	
		    	p.setX(FastMath.wrap(p.getX(),State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));
		    	p.setY(FastMath.wrap(p.getY(),State.instance().getWorld().getInstance(getCharacter().getInstanceId()).getTerrain().getSize()));
		    	
		    	try
		    	{
		    		if(!State.instance().getWorld().getInstance(getCharacter().getInstanceId()).isTileImpassable(p))
		    		{
		    			tto=true;
		    			
		    			getCharacter().setLocation       (p  );
		    			getCharacter().setSubTileLocation(stp);
		    		}
		    	}
		    	catch(Exception e)
		    	{
		    		;
		    	}
		    }
		    else
		    {
		    	getCharacter().setSubTileLocation(stp);
		    }
		    
		    if(tto)
		    {
			    try
			    {
			    	CommandProperties props=new CommandProperties();
			    	
			    	props.setCharacter      ("character",             getCharacter()                     );
			    	props.setPoint          ("location",              getCharacter().getLocation()       );
			    	props.setSubTileLocation("subTileLocation",       getCharacter().getSubTileLocation());
			    	props.setBoolean        ("cancelPath",            false                              );
			    	props.setBoolean        ("tileTransitionOccurred",tto                                );
			    	
			        State.instance().getGame().addIncomingRequest(
			    		"CORE.COMMAND.PLAYER.MOVE",
			    		props
			        );
			    }
			    catch (CommandNotSupportedException e)
			    {
			        e.printStackTrace();
			    }
			    catch (IsNotServerInvokedCommand e)
			    {
			        e.printStackTrace();
			    }
		    }
		}
	}
	
	@Override
	public byte findOrientation()
    {
		if(isDirectionallyMoving())
		{
			if(movementStack.get(0) == IDirectionalMovement.DIRECTION_NORTH)
			{
				return(Character.ORIENTATION_NORTH);
			}
			if(movementStack.get(0) == IDirectionalMovement.DIRECTION_SOUTH)
			{
				return(Character.ORIENTATION_SOUTH);
			}
			if(movementStack.get(0) == IDirectionalMovement.DIRECTION_EAST )
			{
				return(Character.ORIENTATION_EAST );
			}
			if(movementStack.get(0) == IDirectionalMovement.DIRECTION_WEST )
			{
				return(Character.ORIENTATION_WEST );
			}
		}

	    return((byte) character.getOrientation());
    }

	@Override
    public byte[] toBytes()
    {
	    EnhancedDataOutputStream out=null;
	    
		try
		{
		    ByteArrayOutputStream baos=new ByteArrayOutputStream();
		    
		    out=new EnhancedDataOutputStream(baos);

		    if(movementStack.size() == 0)
		    {
		    	 out.writeByte(IDirectionalMovement.DIRECTION_NONE);
		    }
		    else
		    {
		    	 out.writeByte(movementStack.get(0));
		    }
		    
		    return(baos.toByteArray());
		}
		catch(Exception e)
		{
			logger.error(e.getMessage(),e);
		}
		finally
		{
			try{out.close();}catch(Exception e){};
		}
		
		return(null);
    }
}